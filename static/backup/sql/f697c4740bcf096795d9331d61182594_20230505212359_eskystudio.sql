-- Online Database Management SQL Dump
-- 数据库名: eskystudio
-- 生成日期: 2023-05-05 21:23:59
-- PHP 版本: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+08:00";
SET NAMES utf8;

-- --------------------------------------------------------

--
-- 数据库名 `eskystudio`
--

CREATE DATABASE IF NOT EXISTS `eskystudio` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `eskystudio`;

-- --------------------------------------------------------

--
-- 表的结构 `ay_area`
--

DROP TABLE IF EXISTS `ay_area`;
CREATE TABLE `ay_area` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '区域编号',
  `acode` varchar(20) NOT NULL COMMENT '区域编码',
  `pcode` varchar(20) NOT NULL COMMENT '区域父编码',
  `name` varchar(50) NOT NULL COMMENT '区域名称',
  `domain` varchar(100) NOT NULL COMMENT '区域绑定域名',
  `is_default` char(1) NOT NULL DEFAULT '0' COMMENT '是否默认',
  `create_user` varchar(30) NOT NULL COMMENT '添加人员',
  `update_user` varchar(30) NOT NULL COMMENT '更新人员',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ay_area_acode` (`acode`),
  KEY `ay_area_pcode` (`pcode`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_area`
--

INSERT INTO `ay_area` (`id`,`acode`,`pcode`,`name`,`domain`,`is_default`,`create_user`,`update_user`,`create_time`,`update_time`) VALUES
('1','cn','0','中文','','1','admin','admin','2017-11-30 13:55:37','2018-04-13 11:40:49');

-- --------------------------------------------------------

--
-- 表的结构 `ay_company`
--

DROP TABLE IF EXISTS `ay_company`;
CREATE TABLE `ay_company` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '站点编号',
  `acode` varchar(20) NOT NULL COMMENT '区域代码',
  `name` varchar(100) NOT NULL COMMENT '公司名称',
  `address` varchar(200) NOT NULL COMMENT '公司地址',
  `postcode` varchar(6) NOT NULL COMMENT '邮政编码',
  `contact` varchar(10) NOT NULL COMMENT '公司联系人',
  `mobile` varchar(50) NOT NULL COMMENT '手机号码',
  `phone` varchar(50) NOT NULL COMMENT '电话号码',
  `fax` varchar(50) NOT NULL COMMENT '公司传真',
  `email` varchar(30) NOT NULL COMMENT '电子邮箱',
  `qq` varchar(50) NOT NULL COMMENT '公司QQ',
  `weixin` varchar(100) NOT NULL COMMENT '微信图标',
  `blicense` varchar(20) NOT NULL COMMENT '营业执照代码',
  `other` varchar(200) NOT NULL COMMENT '其他信息',
  PRIMARY KEY (`id`),
  KEY `ay_company_acode` (`acode`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_company`
--

INSERT INTO `ay_company` (`id`,`acode`,`name`,`address`,`postcode`,`contact`,`mobile`,`phone`,`fax`,`email`,`qq`,`weixin`,`blicense`,`other`) VALUES
('1','cn','天一伟业互动网络运营中心-济宁主舵者网络科技有限公司','济宁市高新区金宇路红星美凯龙商业中心518室','272100','刘经理','15564768866','0537-2254866','','eskystudio@qq.com','116727967@qq.com','/static/upload/image/20200312/1584017711857778.png','','');

-- --------------------------------------------------------

--
-- 表的结构 `ay_config`
--

DROP TABLE IF EXISTS `ay_config`;
CREATE TABLE `ay_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(30) NOT NULL COMMENT '名称',
  `value` varchar(200) NOT NULL COMMENT '值',
  `type` char(1) NOT NULL DEFAULT '1' COMMENT '配置类型',
  `sorting` int(10) unsigned NOT NULL DEFAULT '255' COMMENT '排序',
  `description` varchar(30) NOT NULL COMMENT '描述文本',
  PRIMARY KEY (`id`),
  KEY `ay_config_name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_config`
--

INSERT INTO `ay_config` (`id`,`name`,`value`,`type`,`sorting`,`description`) VALUES
('1','open_wap','0','1','255','手机版'),
('2','message_check_code','0','1','255','留言验证码'),
('3','smtp_server','smtp.qq.com','2','255','邮件SMTP服务器'),
('4','smtp_port','465','2','255','邮件SMTP端口'),
('5','smtp_ssl','1','1','255','邮件是否安全连接'),
('6','smtp_username','','2','255','邮件发送账号'),
('7','smtp_password','','2','255','邮件发送密码'),
('8','admin_check_code','1','1','255','后台验证码'),
('9','weixin_appid','','2','255','微信APPID'),
('10','weixin_secret','','2','255','微信SECRET'),
('11','message_send_mail','1','1','255','留言发送邮件开关'),
('12','message_send_to','','1','255','留言发送到邮箱'),
('13','api_open','1','2','255','API开关'),
('14','api_auth','1','2','255','API强制认证'),
('15','api_appid','wx0b85ff95240f7174','2','255','API认证用户'),
('16','api_secret','12334356465765867878943','2','255','API认证密钥'),
('17','baidu_zz_token','6sSFtfWEoWr1hW0p','2','255','百度站长密钥'),
('18','baidu_xzh_appid','','2','255','熊掌号appid'),
('19','baidu_xzh_token','','2','255','熊掌号token'),
('20','wap_domain','','2','255','手机绑定域名'),
('21','gzip','0','2','255','GZIP压缩'),
('22','content_tags_replace_num','','2','255','内容关键字替换次数'),
('23','smtp_username_test','','2','255','测试邮箱'),
('24','form_send_mail','0','2','255','表单发送邮件'),
('25','baidu_xzh_type','0','2','255','熊掌号推送类型'),
('26','watermark_open','0','2','255','水印开关'),
('27','watermark_text','PbootCMS','2','255','水印文本'),
('28','watermark_text_font','','2','255','水印文本字体'),
('29','watermark_text_size','20','2','255','水印文本字号'),
('30','watermark_text_color','100,100,100','2','255','水印文本字体颜色'),
('31','watermark_pic','/static/images/logo.png','2','255','水印图片'),
('32','watermark_position','4','2','255','水印位置'),
('33','message_verify','1','2','255','留言审核'),
('34','form_check_code','0','2','255','表单验证码'),
('35','lock_count','5','2','255','登陆锁定阈值'),
('36','lock_time','900','2','255','登录锁定时间'),
('37','url_rule_type','3','2','255','路径类型'),
('38','message_status','1','2','255',''),
('39','form_status','1','2','255',''),
('40','tpl_html_dir','','2','255',''),
('41','ip_deny','','2','255',''),
('42','ip_allow','','2','255',''),
('43','upgrade_branch','3.X','2','255',''),
('44','upgrade_force','0','2','255',''),
('55','spiderlog','1','2','255',''),
('45','url_rule_sort_suffix','0','2','255',''),
('46','close_site','0','2','255',''),
('47','close_site_note','','2','255',''),
('48','lgautosw','1','2','255',''),
('49','to_https','0','2','255',''),
('50','to_main_domain','1','2','255',''),
('51','main_domain','','2','255',''),
('52','content_keyword_replace','','2','255',''),
('53','url_rule_content_path','0','2','255',''),
('54','html_dir','','2','255',''),
('56','sn','BC73173C54,195D808D2A','2','255',''),
('57','sn_user','','2','255',''),
('58','licensecode','QkM3MzE3M0M1NCwxOTVEODA4RDJBLw==C','2','255','');

-- --------------------------------------------------------

--
-- 表的结构 `ay_content`
--

DROP TABLE IF EXISTS `ay_content`;
CREATE TABLE `ay_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `acode` varchar(20) NOT NULL COMMENT '区域',
  `scode` varchar(20) NOT NULL COMMENT '内容栏目',
  `subscode` varchar(20) NOT NULL COMMENT '副栏目',
  `title` varchar(100) NOT NULL COMMENT '标题',
  `titlecolor` varchar(7) NOT NULL COMMENT '标题颜色',
  `subtitle` varchar(100) NOT NULL COMMENT '副标题',
  `filename` varchar(50) NOT NULL COMMENT '自定义文件名',
  `author` varchar(30) NOT NULL COMMENT '作者',
  `source` varchar(30) NOT NULL COMMENT '来源',
  `outlink` varchar(100) NOT NULL COMMENT '外链地址',
  `date` datetime NOT NULL COMMENT '发布日期',
  `ico` varchar(100) NOT NULL COMMENT '缩略图',
  `pics` varchar(1000) NOT NULL COMMENT '多图片',
  `content` mediumtext NOT NULL COMMENT '内容',
  `tags` varchar(500) NOT NULL COMMENT 'tag关键字',
  `enclosure` varchar(100) NOT NULL COMMENT '附件',
  `keywords` varchar(200) NOT NULL COMMENT '关键字',
  `description` varchar(500) NOT NULL COMMENT '描述',
  `sorting` int(10) unsigned NOT NULL DEFAULT '255' COMMENT '内容排序',
  `status` char(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `istop` char(1) NOT NULL DEFAULT '0' COMMENT '是否置顶',
  `isrecommend` char(1) NOT NULL DEFAULT '0' COMMENT '是否推荐',
  `isheadline` char(1) NOT NULL DEFAULT '0' COMMENT '是否头条',
  `visits` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '访问数',
  `likes` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点赞数',
  `oppose` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '反对数',
  `create_user` varchar(30) NOT NULL COMMENT '创建人员',
  `update_user` varchar(20) NOT NULL COMMENT '更新人员',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `gtype` char(1) NOT NULL DEFAULT '4',
  `gid` varchar(20) NOT NULL DEFAULT '',
  `gnote` varchar(100) NOT NULL DEFAULT '',
  `picstitle` varchar(1000) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ay_content_scode` (`scode`),
  KEY `ay_content_subscode` (`subscode`),
  KEY `ay_content_acode` (`acode`),
  KEY `ay_content_filename` (`filename`),
  KEY `ay_content_date` (`date`),
  KEY `ay_content_sorting` (`sorting`),
  KEY `ay_content_status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=297 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_content`
--

INSERT INTO `ay_content` (`id`,`acode`,`scode`,`subscode`,`title`,`titlecolor`,`subtitle`,`filename`,`author`,`source`,`outlink`,`date`,`ico`,`pics`,`content`,`tags`,`enclosure`,`keywords`,`description`,`sorting`,`status`,`istop`,`isrecommend`,`isheadline`,`visits`,`likes`,`oppose`,`create_user`,`update_user`,`create_time`,`update_time`,`gtype`,`gid`,`gnote`,`picstitle`) VALUES
('1','cn','1','','公司简介','#333333','','','admin','本站','','2020-03-13 14:28:42','','','<p>　　<strong><span style=\"color: rgb(255, 0, 0);\">天一伟业(ESKYSTUDIO)</span></strong> 是济宁主舵者网络科技旗下的品牌设计网络事业部，成立于2004年，竭力打造成为济宁地区具有全新创新意识的品牌设计和网站运营团队。<br/>　　我们坚持<strong><span style=\"color: rgb(255, 0, 0);\">“设计创造价值”</span></strong>的执业理念，为每一位客户量身定制，进行原创包装，为企业形象宣传和产品服务的网络推广助力，完美打造新型视觉和网络品牌！&nbsp;<br/>　　我们的宗旨是：力求为客户提供高质量品牌设计，并愿以专业的素养，兢业的态度，科学的流程，为客户提供及时、准确、完善的设计服务,让客户认知其品牌以及产品的价值和潜力，在市场上获得成功。<br/>　　团队拥有资深的艺术设计专家，网络技术人才，品牌推广专员和细致周到的售后服务力量，以完善的设计产品和专业的解决方案为省内外千余家企事业及个人提供了优质服务！</p>','','','','　　天一伟业(ESKYSTUDIO) 是济宁主舵者网络科技旗下的品牌设计网络事业部，成立于2004年，竭力打造成为济宁地区具有全新创新意识的品牌设计和网站运营团队。　　我们坚持“设计创造价值”的执业理念，为每一位客户量身定制，进行原创包装，为企业形象宣传和产品服务的网络推广助力，完美打造新型视觉和网络品牌！　　','255','1','0','0','0','3545','0','0','admin','supersky','2018-04-11 17:26:11','2020-03-13 14:29:54','4','','',''),
('3','cn','11','','联系我们','#333333','','','admin','本站','','2018-04-11 17:31:29','','','<p>官方网站：<a href=\"http://www.pbootcms.com\">www.pbootcms.com</a><br/></p><p>技术交流群： 137083872</p><p><br/></p><p>我们一直秉承大道至简分享便可改变世界的理念，坚持做最简约灵活的PbootCMS开源软件！</p><p>您的每一份帮助都将支持PbootCMS做的更好，走的更远！</p><p>我们一直在坚持不懈地努力，并尽可能让PbootCMS完全开源免费，您的帮助将使我们更有动力和信心^_^！</p><p>扫一扫官网付款码赞助我们，您的支持是开发者不断前进的动力！</p><p><br/></p><p><strong>您的每一份捐赠将用来：</strong></p><p>深入PbootCMS核心的开发、</p><p>做丰富的应用；</p><p>设计更爽的用户界面；</p><p>吸引更多的模板开发者和应用开发者；</p><p>奖励更多优秀贡献者。</p><p>把PbootCMS技术交流群137083872推荐给伱自己有兴趣的群做宣传，也是对我们的帮助哟！~~</p><p><img src=\"/static/upload/image/20180413/1523583018133454.png\"/></p><p><br/></p>','','','','','255','1','0','0','0','4802','0','0','admin','admin','2018-04-11 17:31:29','2018-04-13 09:30:19','4','','',''),
('54','cn','3','','品牌设计','#333333','','','supersky','本站','','2020-04-03 14:57:39','','','','','','','','255','1','0','0','0','3223','0','0','supersky','supersky','2020-04-03 14:57:39','2021-12-23 08:53:03','4','','',''),
('48','cn','5','','软件开发','#333333','','','supersky','本站','','2020-04-02 22:38:46','','','','','','','','255','1','0','0','0','2542','0','0','supersky','supersky','2020-04-02 22:38:46','2020-04-02 22:38:46','4','','',''),
('49','cn','20','','阿里云','#333333','','','supersky','本站','','2020-04-03 14:02:15','/static/upload/image/20200403/1585894155863948.png','','','','','','','255','1','0','0','0','0','0','0','supersky','supersky','2020-04-03 14:02:51','2020-04-03 14:09:16','4','','',''),
('50','cn','20','','北京新网','#333333','','','supersky','本站','','2020-04-03 14:02:53','/static/upload/image/20200403/1585894118661939.png','','','','','','','255','1','0','0','0','0','0','0','supersky','supersky','2020-04-03 14:04:49','2020-04-03 14:08:39','4','','',''),
('51','cn','20','','百度','#333333','','','supersky','本站','','2020-04-03 14:04:50','/static/upload/image/20200403/1585893941985680.png','','','','','','','255','1','0','0','0','0','0','0','supersky','supersky','2020-04-03 14:05:43','2020-04-03 14:05:43','4','','',''),
('52','cn','20','','腾讯','#333333','','','supersky','本站','','2020-04-03 14:05:44','/static/upload/image/20200403/1585894182396252.png','','','','','','','255','1','0','0','0','0','0','0','supersky','supersky','2020-04-03 14:06:31','2020-04-03 14:09:43','4','','',''),
('53','cn','20','','中国联通','#333333','','','supersky','本站','','2020-04-03 14:09:44','/static/upload/image/20200403/1585894814522350.jpg','','','','','','','255','1','0','0','0','0','0','0','supersky','test','2020-04-03 14:20:15','2023-05-05 20:23:15','4','','',''),
('38','cn','16','','营销推广','#333333','','','supersky','本站','','2020-03-18 22:33:29','','','','','','','','255','1','0','0','0','0','0','0','supersky','supersky','2020-03-18 22:33:29','2020-03-18 22:33:29','4','','',''),
('47','cn','7','','微信营销','#333333','','','supersky','本站','','2020-04-02 21:46:30','','','','','','','','255','1','0','0','0','2833','0','0','supersky','supersky','2020-04-02 21:46:30','2020-04-02 21:46:30','4','','',''),
('46','cn','6','','网站建设111','#333333','','','supersky','本站','','2020-04-02 20:58:27','','','','','','','','255','1','0','0','0','3278','0','0','supersky','test','2020-04-02 20:58:27','2023-05-05 20:48:36','4','','',''),
('58','cn','13','','每一个偶然都会引起一个必然--蝴蝶效应如是说123111111111','#333333','','','supersky','本站','','2012-06-06 18:05:58','/static/upload/image/20200407/1586253979112981.jpg','','<p><img src=\"/static/upload/image/20200407/1586253979112981.jpg\" title=\"1586253979112981.jpg\" alt=\"201206061531428476.jpg\"/></p><p>　　我们需要的健康持久的发展成长，</p><p>　　我们需要竭尽全力的去满足客户的需求，</p><p>　　并获得客户的认可和信任，</p><p>　　所以，善待客户</p><p>　　真正的站在客户的角度上去思考</p><p>　　注重每一个细节、细节和细节！</p><p>　　用120%的努力，尽量去避免失误！</p><p>　　每一个孜孜不倦的努力，最终换来的肯定是我们坚持已久的成功！</p><p><br/></p><p>&nbsp; &nbsp; &nbsp; &nbsp;天一伟业，一直在努力！</p>','','','','我们需要的健康持久的发展成长，我们需要竭尽全力的去满足客户的需求，并获得客户的认可和信任，所以，善待客户真正的站在客户的角度上去思考注重每一个细节、细节和细节！用120%的努力，尽量去避免失误！每一个孜孜不倦的努力，最终换来的肯定是我们坚持已久的成功！','255','1','0','0','0','1458','0','0','supersky','test','2020-04-07 18:07:32','2023-05-05 20:50:28','4','0','',''),
('59','cn','12','','山东雅正印刷有限公司--设计案例','#333333','','','supersky','本站','/arthon','2020-04-07 18:07:34','','','','','','','','255','1','0','0','0','46','0','0','supersky','supersky','2020-04-07 18:32:45','2020-04-07 18:32:45','4','','',''),
('121','cn','13','','新智慧零售微信小程序小店','#333333','','','supersky','本站','','2020-04-18 16:31:50','','','<p>2020年天一伟业隆重推出：新智慧零售微信小程序小店，小门槛，大跨跃；适用于各类大中小型在线商城，分销团购秒杀，各类营销活动，各种获客模式；欢迎扫码体验，欢迎来电咨询砸单！</p><p><br/></p><p><img src=\"/static/upload/image/20200418/1587198836201692.jpg\" style=\"width: 512px; height: 512px;\" title=\"1587198836201692.jpg\" width=\"512\" height=\"512\" alt=\"1587198836201692.jpg\"/></p><p><img src=\"/static/upload/image/20200418/1587198836132407.jpg\" style=\"width: 512px;\" title=\"1587198836132407.jpg\"/></p><p><img src=\"/static/upload/image/20200418/1587198836872460.png\" style=\"width: 512px;\" title=\"1587198836872460.png\"/></p><p><img src=\"/static/upload/image/20200418/1587198837456584.png\" style=\"width: 512px;\" title=\"1587198837456584.png\"/></p><p><img src=\"/static/upload/image/20200418/1587198837199662.png\" style=\"width: 512px;\" title=\"1587198837199662.png\"/></p><p><br/></p>','','','','2020年天一伟业隆重推出：新智慧零售微信小程序小店，小门槛，大跨跃；适用于各类大中小型在线商城，分销团购秒杀，各类营销活动，各种获客模式；欢迎扫码体验，欢迎来电咨询砸单！','255','1','0','0','0','1519','0','0','supersky','supersky','2020-04-18 16:34:46','2020-04-19 15:07:44','4','','',''),
('180','cn','19','','晋之风生态农业','#333333','以文旅产业促进乡村振兴','','Eskystudio','本站','','2022-11-30 17:11:26','/static/upload/image/20230131/1675156880306062.png','','','','','','晋之风生态农业科技有限公司成立于2020年，注册资本陆仟万元。主要经营范围是：农业技术服务、农业观光项目开发、矿区环境修复治理、土壤的整理和治理、荒山荒坡修复治理、园林绿化工程等。是一家致力于生态修复，农业一二三产融合发展，促进乡村振兴为办企宗旨的民营企业。2020年，我公司决策把拥有的尧都区土门镇王汾村17','255','1','1','0','0','59','0','0','supersky','supersky','2023-01-31 17:22:25','2023-02-18 17:06:23','4','0','',''),
('181','cn','19','','绿郡荟','#333333','专筑.尚品生活','','Eskystudio','本站','','2023-01-01 14:28:53','/static/upload/image/20230218/1676703383454226.jpg','','<p style=\"text-align: center;\">绿郡荟</p><p style=\"text-align: center;\">专筑.尚品生活</p><p style=\"text-align: center;\"><img src=\"/static/upload/image/20230218/1676703375711680.png\" alt=\"微信截图_20230218145509.png\"/></p>','','','','绿郡荟专筑.尚品生活','255','1','1','1','0','113','0','0','supersky','supersky','2023-02-18 14:56:34','2023-02-18 17:28:54','4','0','',''),
('193','cn','22','','小程序开发','#333333','','','Eskystudio','本站','','2023-02-25 15:46:52','','','','','','','','255','1','0','0','0','264','0','0','supersky','supersky','2023-02-25 15:46:52','2023-02-25 15:46:52','4','0','',''),
('295','cn','21','','小程序开发现状及技术特点111','#333333','','','Eskystudio','本站','','2023-02-27 16:00:20','','','<p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; outline: none; box-sizing: border-box; color: rgb(46, 46, 46); font-family: &quot;Microsoft YaHei&quot;, Arial, Helvetica, sans-serif; font-size: 16px; white-space: normal; background-color: rgb(255, 255, 255);\">在传统企业的线下发展困境中，主要是邀客成本和营销环境的限制，而这些则是线上销售的优势，因此很多传统企业开始考虑打造自己的小程序，从而能够积极布局移动互联网的行业市场。下面，就给大家梳理一下<a href=\"https://www.eskystudio.com/?applet/\" target=\"_self\" textvalue=\"武汉小程序开发\"><strong style=\"margin: 0px; padding: 0px; border: 0px; outline: none; box-sizing: border-box;\">武汉小程序开发</strong></a>的行业现状，帮助大家理解武汉小程序开发技术特点。</p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; outline: none; box-sizing: border-box; color: rgb(46, 46, 46); font-family: &quot;Microsoft YaHei&quot;, Arial, Helvetica, sans-serif; font-size: 16px; white-space: normal; background-color: rgb(255, 255, 255);\">&nbsp; &nbsp; &nbsp; 武汉小程序开发功能和定位</p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; outline: none; box-sizing: border-box; color: rgb(46, 46, 46); font-family: &quot;Microsoft YaHei&quot;, Arial, Helvetica, sans-serif; font-size: 16px; white-space: normal; background-color: rgb(255, 255, 255);\">&nbsp; &nbsp; &nbsp; 企业用户选择小程序开发公司，其前提条件就是这款小程序具备足够的市场空间，能够满足企业用户的营销宣传和消费者的浏览需求，才能具备小程序开发的资格,继而具有成功的可能。所以企业用户在选择小程序开发之前，首先要对将来所面对的市场和消费者进行分析，确定市场目标定位和用户小程序开发需求，从而制作出用户感兴趣的小程序。</p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; outline: none; box-sizing: border-box; color: rgb(46, 46, 46); font-family: &quot;Microsoft YaHei&quot;, Arial, Helvetica, sans-serif; font-size: 16px; white-space: normal; background-color: rgb(255, 255, 255);\">&nbsp; &nbsp; &nbsp; 武汉小程序开发场景</p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; outline: none; box-sizing: border-box; color: rgb(46, 46, 46); font-family: &quot;Microsoft YaHei&quot;, Arial, Helvetica, sans-serif; font-size: 16px; white-space: normal; background-color: rgb(255, 255, 255);\">&nbsp; &nbsp; &nbsp; 通常情况下，小程序开发是基于移动互联网应用场景，主要是针对全国十亿用户的流量红利，包括但不限于电商类小程序、应用工具类小程序、应用管理类小程序等实际需要的小程序类型，它可以帮助企业用户节省大量的时间，因为传统企业的产品种类繁多，销售报价工作量大，企业用户考虑小程序开发公司时，着重需要考虑小程序开发功能，帮助企业梳理所有产品目录和产品品类，实现产品销售的精细化管理。</p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; outline: none; box-sizing: border-box; color: rgb(46, 46, 46); font-family: &quot;Microsoft YaHei&quot;, Arial, Helvetica, sans-serif; font-size: 16px; white-space: normal; background-color: rgb(255, 255, 255);\">&nbsp; &nbsp; &nbsp; 武汉小程序开发性能参数</p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; outline: none; box-sizing: border-box; color: rgb(46, 46, 46); font-family: &quot;Microsoft YaHei&quot;, Arial, Helvetica, sans-serif; font-size: 16px; white-space: normal; background-color: rgb(255, 255, 255);\">&nbsp; &nbsp; &nbsp; 在小程序开发公司的定制开发过程中，具体的产品和参数都是在明确的企业业务逻辑的基础上进行的，这就要求传统企业在考虑小程序开发公司时，还必须考虑小程序开发公司的行业背景、专业技术水平和团队成员实力，尤其是必须具有传统企业的行业背景，这样才能做到小程序开发的个性化定制，满足企业用户的个性化营销需求。</p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; outline: none; box-sizing: border-box; color: rgb(46, 46, 46); font-family: &quot;Microsoft YaHei&quot;, Arial, Helvetica, sans-serif; font-size: 16px; white-space: normal; background-color: rgb(255, 255, 255);\">&nbsp; &nbsp; &nbsp; 以上就是武汉小程序开发的核心要素梳理，要求企业用户在考虑小程序开发公司时，必须能够充分了解自身的小程序开发需求点，结合采购预算，考虑小程序开发公司的同行业成功案例，选择心仪的小程序开发公司。</p><p><br/></p>','','','','在传统企业的线下发展困境中，主要是邀客成本和营销环境的限制，而这些则是线上销售的优势，因此很多传统企业开始考虑打造自己的小程序，从而能够积极布局移动互联网的行业市场。下面，就给大家梳理一下武汉小程序开发的行业现状，帮助大家理解武汉小程序开发技术特点。 武汉小程序开发功能和定位&nbsp','255','1','0','0','0','65','0','0','supersky','test','2023-02-27 16:00:55','2023-05-05 20:38:56','4','0','',''),
('296','cn','13','','ceshi-111111111','#333333','','','test','本站','','2023-05-05 21:06:06','/static/upload/image/20230505/1683292025264273.jpg','','<p><video class=\"edui-upload-video  vjs-default-skin video-js\" controls=\"\" preload=\"none\" width=\"420\" height=\"280\" src=\"/static/upload/video/20230505/1683291990166349.mp4\" data-setup=\"{}\"><source src=\"/static/upload/video/20230505/1683291990166349.mp4\" type=\"video/mp4\"/></video></p>','','','','','255','1','0','0','0','2','0','0','test','test','2023-05-05 21:07:07','2023-05-05 21:07:07','4','0','','');

-- --------------------------------------------------------

--
-- 表的结构 `ay_content_ext`
--

DROP TABLE IF EXISTS `ay_content_ext`;
CREATE TABLE `ay_content_ext` (
  `extid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contentid` int(10) unsigned NOT NULL,
  `ext_type` varchar(100) DEFAULT NULL COMMENT '类型',
  `ext_url` varchar(200) DEFAULT NULL COMMENT '网址',
  `ext_bigpic` varchar(200) DEFAULT NULL COMMENT '形象大图',
  `ext_logo` varchar(200) DEFAULT NULL COMMENT 'Logo',
  `ext_ipad` varchar(200) DEFAULT NULL COMMENT 'iPad演示',
  `ext_iphone` varchar(200) DEFAULT NULL COMMENT '手机界面',
  `ext_index` varchar(200) DEFAULT NULL COMMENT '首页截图',
  PRIMARY KEY (`extid`),
  KEY `ay_content_ext_contentid` (`contentid`)
) ENGINE=MyISAM AUTO_INCREMENT=121 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_content_ext`
--

INSERT INTO `ay_content_ext` (`extid`,`contentid`,`ext_type`,`ext_url`,`ext_bigpic`,`ext_logo`,`ext_ipad`,`ext_iphone`,`ext_index`) VALUES
('119','180','响应式网站,企业门户,营销型','http://www.jzfny.com','/static/upload/image/20230131/1675156818250050.png','/static/upload/image/20230131/1675156821270707.png','/static/upload/image/20230131/1675156849901388.png','/static/upload/image/20230131/1675156853340788.png','/static/upload/image/20230131/1675156857360475.png'),
('120','181','微信程序,小程序,软件开发','','/static/upload/image/20230218/1676703322597856.jpg','/static/upload/image/20230218/1676703464651419.png','/static/upload/image/20230218/1676712531251228.jpg','/static/upload/image/20230218/1676712300903122.jpg','/static/upload/image/20230218/1676703322597856.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `ay_content_sort`
--

DROP TABLE IF EXISTS `ay_content_sort`;
CREATE TABLE `ay_content_sort` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `acode` varchar(20) NOT NULL COMMENT '区域编码',
  `mcode` varchar(20) NOT NULL COMMENT '内容模型编码',
  `pcode` varchar(20) NOT NULL COMMENT '父编码',
  `scode` varchar(20) NOT NULL COMMENT '分类编码',
  `name` varchar(100) NOT NULL COMMENT '分类名称',
  `listtpl` varchar(50) NOT NULL COMMENT '列表页模板',
  `contenttpl` varchar(50) NOT NULL COMMENT '内容页模板',
  `status` char(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `outlink` varchar(100) NOT NULL COMMENT '转外链接',
  `subname` varchar(200) NOT NULL COMMENT '附加名称',
  `ico` varchar(100) NOT NULL COMMENT '分类缩略图',
  `pic` varchar(100) NOT NULL COMMENT '分类大图',
  `title` varchar(100) NOT NULL COMMENT 'seo标题',
  `keywords` varchar(200) NOT NULL COMMENT '分类关键字',
  `description` varchar(500) NOT NULL COMMENT '分类描述',
  `filename` varchar(30) NOT NULL COMMENT '自定义文件名',
  `sorting` int(10) unsigned NOT NULL DEFAULT '255' COMMENT '排序',
  `create_user` varchar(30) NOT NULL COMMENT '创建人员',
  `update_user` varchar(30) NOT NULL COMMENT '更新人员',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `gtype` char(1) NOT NULL DEFAULT '4',
  `gid` varchar(20) NOT NULL DEFAULT '',
  `gnote` varchar(100) NOT NULL DEFAULT '',
  `def1` varchar(1000) NOT NULL DEFAULT '',
  `def2` varchar(1000) NOT NULL DEFAULT '',
  `def3` varchar(1000) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ay_content_sort_scode` (`scode`),
  KEY `ay_content_sort_pcode` (`pcode`),
  KEY `ay_content_sort_acode` (`acode`),
  KEY `ay_content_sort_mcode` (`mcode`),
  KEY `ay_content_sort_filename` (`filename`),
  KEY `ay_content_sort_sorting` (`sorting`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_content_sort`
--

INSERT INTO `ay_content_sort` (`id`,`acode`,`mcode`,`pcode`,`scode`,`name`,`listtpl`,`contenttpl`,`status`,`outlink`,`subname`,`ico`,`pic`,`title`,`keywords`,`description`,`filename`,`sorting`,`create_user`,`update_user`,`create_time`,`update_time`,`gtype`,`gid`,`gnote`,`def1`,`def2`,`def3`) VALUES
('1','cn','1','0','1','关于我们','','aboutus.html','1','','网站建设「一站式」服务商','','','','','','aboutus','9','admin','supersky','2018-04-11 17:26:11','2023-02-25 15:25:46','4','','','','',''),
('2','cn','2','0','2','新闻中心','newslist.html','news.html','1','','了解最新公司动态及行业资讯','','/images/banner-news.jpg','','','我们只致于力做好一件事：给客户提供基于互联网技术的数字化解决方案
虽然它包括视觉设计、用户行为研究、交互效果、技术服务以及基于移动端以及传统平台的营销支持','article','7','admin','supersky','2018-04-11 17:26:46','2023-02-25 15:25:46','4','','','','',''),
('3','cn','1','0','3','品牌设计','','design.html','1','','设计创造价值','/static/upload/image/20200420/1587376282554876.jpg','/static/upload/image/20200403/1585897399358128.jpg','','','提升企业品牌效应','design','5','admin','supersky','2018-04-11 17:27:05','2023-02-25 15:25:46','4','','','','',''),
('4','cn','2','2','4','技术研究','newslist.html','news.html','1','','每天发现不一样的自己','','/images/banner-news.jpg','','','','industry','255','admin','supersky','2018-04-11 17:27:30','2023-02-25 15:25:46','4','','','','',''),
('5','cn','1','0','5','软件开发','','software.html','1','','根据需求，定制各类互联网、物联网软件开发','/static/upload/image/20200420/1587376228786258.jpg','/images/software.jpg','','','专业定制开发各类软件CRM、OA、ERP、设备管理软件、库存管理软件、阿米巴、等各类软件及数据可视化软件等定制服务开发。','software','4','admin','supersky','2018-04-11 17:27:54','2023-02-25 15:25:46','4','','','','',''),
('6','cn','1','0','6','网站建设','','web_build.html','1','','品牌网站建设 设计创造价值','','/images/ban2.jpg','','','我们信仰并一直坚持，用品牌思维为客户打造真正有价值的互联网平台!','website','1','admin','supersky','2018-04-11 17:28:19','2023-02-25 15:25:46','4','','','','',''),
('7','cn','1','0','7','微信营销','','mh5.html','1','','微信公众号/小程序/H5高端定制开发','images/banner-mh5-m.jpg','images/banner-mh5.jpg','','','让客户随时随地能与您产生联系！','weixin','2','admin','supersky','2018-04-11 17:28:38','2023-02-25 15:25:46','4','','','','',''),
('9','cn','5','0','9','招贤纳士','joblist.html','job.html','0','','诚聘优秀人士加入我们的团队','','','','','','job','8','admin','supersky','2018-04-11 17:30:02','2023-02-25 15:25:46','4','','','','',''),
('11','cn','1','0','11','联系合作','','contact.html','1','','能为您服务是我们的荣幸！','','/images/contact.jpg','','','让我们的努力惊喜每一位访客！','contact','10','admin','supersky','2018-04-11 17:31:29','2023-02-25 15:25:46','4','','','','',''),
('12','cn','2','2','12','品牌故事','newslist.html','news.html','1','','是你的故事，也是我的故事','','/images/banner-news.jpg','','','','story','255','supersky','supersky','2020-03-13 22:44:00','2023-02-25 15:25:46','4','','','','',''),
('13','cn','2','2','13','天一观点','newslist.html','news.html','1','','有观点，有主见','','/images/banner-news.jpg','','','','idea','255','supersky','supersky','2020-03-13 22:45:24','2023-02-25 15:25:46','4','','','','',''),
('20','cn','5','1','20','合作伙伴','clientlist.html','about.html','1','','同心.共赢','','/images/partner.jpg','','','对于信任我们的客户，我想说的是：这不仅仅是生意上的关系，而是共同成长的过程；合作带来的远远不止是利润，
更多的是经验上的积累和更广阔的发展空间，而这些，才是无价的财富','','255','supersky','supersky','2020-04-03 14:02:11','2023-02-25 15:25:46','4','','','','',''),
('16','cn','1','6','16','营销推广','','about.html','1','','','','','','','','marketing','255','supersky','supersky','2020-03-18 22:33:29','2023-02-25 15:25:46','4','','','','',''),
('19','cn','3','0','19','经典案例','productlist.html','product.html','1','','品牌网站建设 设计创造价值','','/images/ban2.jpg','','','我们的高品质网站建设整合解决方案结合了当前互联网品牌建设经验和整合营销的理念，并将策略和执行紧密结合，不断评估优化我们的网站设计方案，为客户提供一体化的互联网品牌整合方案！让客户满意是我们工作的目标，不断超越客户的期望值来自于我们对这个行业的热爱。我们把公司的定位明确在高端品牌网站平台建设服务商的位置上，我们立志把合适的技术通过高效、便捷的方式提供给客户，为您的企业电子商务化提供最简单的解决方案。','site-case','6','supersky','supersky','2020-04-02 20:57:38','2023-02-25 15:25:46','4','','','','',''),
('21','cn','2','2','21','小程序开发','newslist.html','news.html','1','','微信小程序开发，共享14亿微信用户！','','/images/banner-news.jpg','','','','','1','supersky','supersky','2023-02-20 10:27:19','2023-02-25 15:25:46','4','0','','','',''),
('22','cn','1','0','22','小程序开发','','applet.html','1','','','','','','','','applet','3','supersky','supersky','2023-02-25 15:25:04','2023-02-25 15:51:02','4','0','','','','');

-- --------------------------------------------------------

--
-- 表的结构 `ay_diy_telephone`
--

DROP TABLE IF EXISTS `ay_diy_telephone`;
CREATE TABLE `ay_diy_telephone` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `create_time` datetime NOT NULL,
  `tel` varchar(20) DEFAULT NULL COMMENT '电话号码',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ay_extfield`
--

DROP TABLE IF EXISTS `ay_extfield`;
CREATE TABLE `ay_extfield` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `mcode` varchar(20) NOT NULL COMMENT '模型编码',
  `name` varchar(30) NOT NULL COMMENT '字段名称',
  `type` char(1) NOT NULL COMMENT '字段类型',
  `value` varchar(500) NOT NULL COMMENT '单选或多选值',
  `description` varchar(30) NOT NULL COMMENT '描述文本',
  `sorting` int(11) NOT NULL COMMENT '排序',
  PRIMARY KEY (`id`),
  KEY `ay_extfield_mcode` (`mcode`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_extfield`
--

INSERT INTO `ay_extfield` (`id`,`mcode`,`name`,`type`,`value`,`description`,`sorting`) VALUES
('2','3','ext_type','4','集团网站,响应式网站,电子商务,企业门户,政务平台,工程机械,H5网页设计制作,中国风,营销型,高端品牌,医疗医药,微信公众平台,国际外贸,微信程序,小程序,软件开发,教育系统,多语言,阿里商铺','类型','255'),
('4','3','ext_url','1','','网址11','1'),
('5','3','ext_bigpic','5','','形象大图123','2'),
('6','3','ext_logo','5','','Logo','3'),
('7','3','ext_ipad','5','','iPad演示','255'),
('8','3','ext_iphone','5','','手机界面','255'),
('9','3','ext_index','5','','首页截图','255');

-- --------------------------------------------------------

--
-- 表的结构 `ay_form`
--

DROP TABLE IF EXISTS `ay_form`;
CREATE TABLE `ay_form` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `fcode` varchar(20) NOT NULL COMMENT '表单编码',
  `form_name` varchar(30) NOT NULL COMMENT '表单名称',
  `table_name` varchar(30) NOT NULL COMMENT '表名称',
  `create_user` varchar(30) NOT NULL COMMENT '添加人员',
  `update_user` varchar(30) NOT NULL COMMENT '更新人员',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ay_form_fcode` (`fcode`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_form`
--

INSERT INTO `ay_form` (`id`,`fcode`,`form_name`,`table_name`,`create_user`,`update_user`,`create_time`,`update_time`) VALUES
('1','1','在线留言','ay_message','admin','admin','2018-04-11 17:31:29','2018-04-11 17:31:29'),
('2','2','搜集电话','ay_diy_telephone','admin','admin','2018-11-30 15:17:40','2018-11-30 15:17:40');

-- --------------------------------------------------------

--
-- 表的结构 `ay_form_field`
--

DROP TABLE IF EXISTS `ay_form_field`;
CREATE TABLE `ay_form_field` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `fcode` varchar(20) NOT NULL COMMENT '表单编码',
  `name` varchar(30) NOT NULL COMMENT '字段名称',
  `length` int(10) unsigned NOT NULL COMMENT '字段长度',
  `required` char(1) NOT NULL DEFAULT '0' COMMENT '是否必填',
  `description` varchar(30) NOT NULL COMMENT '描述文本',
  `sorting` int(10) unsigned NOT NULL DEFAULT '255' COMMENT '排序',
  `create_user` varchar(30) NOT NULL COMMENT '添加人员',
  `update_user` varchar(30) NOT NULL COMMENT '更新人员',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `ay_form_field_fcode` (`fcode`),
  KEY `ay_form_field_sorting` (`sorting`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_form_field`
--

INSERT INTO `ay_form_field` (`id`,`fcode`,`name`,`length`,`required`,`description`,`sorting`,`create_user`,`update_user`,`create_time`,`update_time`) VALUES
('1','1','contacts','10','1','联系人','255','admin','admin','2018-07-14 18:24:02','2018-07-15 17:47:43'),
('2','1','mobile','12','1','手机','255','admin','admin','2018-07-14 18:24:02','2018-07-15 17:47:44'),
('3','1','content','500','1','内容','255','admin','admin','2018-07-14 18:24:02','2018-07-15 17:47:45'),
('4','2','tel','20','1','电话号码','255','admin','admin','2018-11-30 15:18:00','2018-11-30 15:18:00');

-- --------------------------------------------------------

--
-- 表的结构 `ay_label`
--

DROP TABLE IF EXISTS `ay_label`;
CREATE TABLE `ay_label` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `value` varchar(500) NOT NULL COMMENT '值',
  `type` char(1) NOT NULL DEFAULT '1' COMMENT '字段类型',
  `description` varchar(30) NOT NULL COMMENT '描述',
  `create_user` varchar(30) NOT NULL COMMENT '创建人员',
  `update_user` varchar(20) NOT NULL COMMENT '更新人员',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_label`
--

INSERT INTO `ay_label` (`id`,`name`,`value`,`type`,`description`,`create_user`,`update_user`,`create_time`,`update_time`) VALUES
('1','downlink','https://gitee.com/hnaoyun/PbootCMS/releases','1','下载地址','admin','admin','2018-04-11 16:52:19','2018-04-30 15:05:00');

-- --------------------------------------------------------

--
-- 表的结构 `ay_link`
--

DROP TABLE IF EXISTS `ay_link`;
CREATE TABLE `ay_link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '序号',
  `acode` varchar(20) NOT NULL COMMENT '区域编码',
  `gid` int(10) unsigned NOT NULL COMMENT '分组序号',
  `name` varchar(50) NOT NULL COMMENT '链接名称',
  `link` varchar(100) NOT NULL COMMENT '跳转链接',
  `logo` varchar(100) NOT NULL COMMENT '图片地址',
  `sorting` int(11) NOT NULL COMMENT '排序',
  `create_user` varchar(30) NOT NULL COMMENT '创建人员',
  `update_user` varchar(30) NOT NULL COMMENT '更新人员',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `ay_link_acode` (`acode`),
  KEY `ay_link_gid` (`gid`),
  KEY `ay_link_sorting` (`sorting`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_link`
--

INSERT INTO `ay_link` (`id`,`acode`,`gid`,`name`,`link`,`logo`,`sorting`,`create_user`,`update_user`,`create_time`,`update_time`) VALUES
('1','cn','1','济宁网站建设','https://www.eskystudio.com','','1','admin','supersky','2018-04-12 10:53:06','2020-04-11 11:05:02'),
('2','cn','1','济宁高端网站设计','https://www.eskystudio.com','','2','supersky','supersky','2020-04-11 11:05:14','2020-04-11 11:05:14'),
('3','cn','1','济宁做网站','https://www.eskystudio.com','','3','supersky','supersky','2020-04-11 11:06:49','2020-04-11 11:06:49'),
('4','cn','1','济宁软件开发公司','http://www.eskysoft.cn','','255','supersky','supersky','2020-07-22 18:58:03','2020-07-22 18:58:03'),
('5','cn','1','济宁微信小程序开发','http://www.eskysoft.cn','','255','supersky','supersky','2020-07-22 18:58:11','2020-07-22 18:58:11'),
('6','cn','1','济宁网站建设','http://www.eskysoft.cn/','','255','supersky','supersky','2020-07-22 18:58:23','2020-07-22 18:58:23'),
('7','cn','1','济宁主舵者网络科技','http://www.jnzdz.cn/','','255','supersky','supersky','2020-07-22 18:58:42','2020-07-22 18:58:42'),
('8','cn','1','山东运河文创展览展示有限公司','http://www.sdyhwc.cn/','','255','supersky','supersky','2021-04-13 09:12:04','2021-04-13 09:12:04'),
('9','cn','1','山东乐音飞扬文化传播有限公司','http://www.sdyyfy.cn/','','255','supersky','supersky','2021-04-13 09:12:19','2021-04-13 09:12:19');

-- --------------------------------------------------------

--
-- 表的结构 `ay_member`
--

DROP TABLE IF EXISTS `ay_member`;
CREATE TABLE `ay_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ucode` varchar(20) NOT NULL,
  `username` varchar(100) NOT NULL,
  `nickname` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `headpic` varchar(200) NOT NULL,
  `status` char(1) NOT NULL,
  `gid` varchar(20) NOT NULL,
  `wxid` varchar(50) NOT NULL,
  `qqid` varchar(50) NOT NULL,
  `wbid` varchar(50) NOT NULL,
  `score` int(10) unsigned NOT NULL DEFAULT '0',
  `register_time` datetime NOT NULL,
  `login_count` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login_ip` varchar(11) NOT NULL,
  `last_login_time` varchar(11) NOT NULL,
  `sex` varchar(2) NOT NULL DEFAULT '',
  `birthday` varchar(20) NOT NULL DEFAULT '',
  `telephone` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `qq` varchar(15) NOT NULL DEFAULT '',
  `useremail` varchar(50) NOT NULL DEFAULT '',
  `usermobile` varchar(11) NOT NULL DEFAULT '',
  `activation` char(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ay_member_ucode` (`ucode`),
  UNIQUE KEY `ay_member_username` (`username`),
  KEY `ay_member_gid` (`gid`),
  KEY `ay_member_wxid` (`wxid`),
  KEY `ay_member_qqid` (`qqid`),
  KEY `ay_member_wbid` (`wbid`),
  KEY `ay_member_useremail` (`useremail`),
  KEY `ay_member_usermobile` (`usermobile`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ay_member_comment`
--

DROP TABLE IF EXISTS `ay_member_comment`;
CREATE TABLE `ay_member_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `contentid` int(10) unsigned NOT NULL,
  `comment` varchar(1000) NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `puid` int(10) unsigned NOT NULL,
  `likes` int(10) unsigned NOT NULL DEFAULT '0',
  `oppose` int(10) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL,
  `user_ip` varchar(11) NOT NULL,
  `user_os` varchar(30) NOT NULL,
  `user_bs` varchar(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_user` varchar(30) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ay_member_comment_pid` (`pid`),
  KEY `ay_member_comment_contentid` (`contentid`),
  KEY `ay_member_comment_uid` (`uid`),
  KEY `ay_member_comment_puid` (`puid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ay_member_field`
--

DROP TABLE IF EXISTS `ay_member_field`;
CREATE TABLE `ay_member_field` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `length` int(10) unsigned NOT NULL,
  `required` char(1) NOT NULL,
  `description` varchar(30) NOT NULL,
  `sorting` int(10) unsigned NOT NULL,
  `status` char(1) NOT NULL,
  `create_user` varchar(30) NOT NULL,
  `update_user` varchar(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_member_field`
--

INSERT INTO `ay_member_field` (`id`,`name`,`length`,`required`,`description`,`sorting`,`status`,`create_user`,`update_user`,`create_time`,`update_time`) VALUES
('1','sex','2','0','性别','255','1','admin','admin','2020-06-25 00:00:00','2020-06-25 00:00:00'),
('2','birthday','20','0','生日','255','1','admin','admin','2020-06-25 00:00:00','2020-06-25 00:00:00'),
('3','qq','15','0','QQ','255','1','admin','admin','2020-06-25 00:00:00','2020-06-25 00:00:00');

-- --------------------------------------------------------

--
-- 表的结构 `ay_member_group`
--

DROP TABLE IF EXISTS `ay_member_group`;
CREATE TABLE `ay_member_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gcode` varchar(20) NOT NULL,
  `gname` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `status` varchar(1) NOT NULL,
  `lscore` int(10) unsigned NOT NULL DEFAULT '0',
  `uscore` int(10) unsigned NOT NULL DEFAULT '0',
  `create_user` varchar(30) NOT NULL,
  `update_user` varchar(30) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ay_member_group_gcode` (`gcode`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_member_group`
--

INSERT INTO `ay_member_group` (`id`,`gcode`,`gname`,`description`,`status`,`lscore`,`uscore`,`create_user`,`update_user`,`create_time`,`update_time`) VALUES
('1','1','初级会员','初级会员具备基本的权限','1','0','999','admin','admin','2020-06-25 00:00:00','2020-06-25 00:00:00'),
('2','2','中级会员','中级会员具备部分特殊权限','1','1000','9999','admin','admin','2020-06-25 00:00:00','2020-06-25 00:00:00'),
('3','3','高级会员','高级会员具备全部特殊权限','1','10000','4294967295','admin','admin','2020-06-25 00:00:00','2020-06-25 00:00:00');

-- --------------------------------------------------------

--
-- 表的结构 `ay_menu`
--

DROP TABLE IF EXISTS `ay_menu`;
CREATE TABLE `ay_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '菜单编号',
  `mcode` varchar(20) NOT NULL COMMENT '菜单编码',
  `pcode` varchar(20) NOT NULL COMMENT '上级菜单',
  `name` varchar(50) NOT NULL COMMENT '菜单名称',
  `url` varchar(100) NOT NULL COMMENT '菜单地址',
  `sorting` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '菜单排序',
  `status` char(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
  `shortcut` char(1) NOT NULL DEFAULT '0' COMMENT '桌面图标',
  `ico` varchar(30) NOT NULL COMMENT '菜单图标',
  `create_user` varchar(30) NOT NULL COMMENT '创建人员',
  `update_user` varchar(30) NOT NULL COMMENT '更新人员',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ay_menu_mcode` (`mcode`),
  KEY `ay_menu_pcode` (`pcode`),
  KEY `ay_menu_sorting` (`sorting`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_menu`
--

INSERT INTO `ay_menu` (`id`,`mcode`,`pcode`,`name`,`url`,`sorting`,`status`,`shortcut`,`ico`,`create_user`,`update_user`,`create_time`,`update_time`) VALUES
('1','M101','0','系统管理','/admin/M101/index','900','1','0','fa-cog','admin','admin','0000-00-00 00:00:00','2018-04-30 14:52:57'),
('2','M102','M101','数据区域','/admin/Area/index','901','1','1','fa-sitemap','admin','admin','0000-00-00 00:00:00','2018-04-30 14:54:23'),
('3','M103','M101','系统菜单','/admin/Menu/index','902','0','0','fa-bars','admin','admin','0000-00-00 00:00:00','2018-04-30 14:54:35'),
('4','M104','M101','系统角色','/admin/Role/index','903','1','1','fa-hand-stop-o','admin','admin','0000-00-00 00:00:00','2018-04-30 14:54:43'),
('5','M105','M101','系统用户','/admin/User/index','904','1','1','fa-users','admin','admin','0000-00-00 00:00:00','2018-04-30 14:54:51'),
('6','M106','M101','系统日志','/admin/Syslog/index','905','1','1','fa-history','admin','admin','0000-00-00 00:00:00','2018-04-30 14:55:00'),
('7','M107','M101','类型管理','/admin/Type/index','906','0','0','fa-tags','admin','admin','0000-00-00 00:00:00','2018-04-30 14:55:13'),
('8','M108','M101','数据库管理','/admin/Database/index','907','1','1','fa-database','admin','admin','0000-00-00 00:00:00','2018-04-30 14:55:24'),
('9','M109','M101','服务器信息','/admin/Site/server','908','1','1','fa-info-circle','admin','admin','0000-00-00 00:00:00','2018-04-30 14:55:34'),
('10','M110','0','基础内容','/admin/M110/index','300','1','0','fa-sliders','admin','admin','2017-11-28 11:13:05','2018-04-30 14:48:29'),
('11','M111','M110','站点信息','/admin/Site/index','301','1','1','fa-cog','admin','admin','0000-00-00 00:00:00','2018-04-07 18:45:57'),
('12','M112','M110','公司信息','/admin/Company/index','302','1','1','fa-copyright','admin','admin','0000-00-00 00:00:00','2018-04-07 18:46:09'),
('29','M129','M110','内容栏目','/admin/ContentSort/index','303','1','1','fa-bars','admin','admin','2017-12-26 10:42:40','2018-04-07 18:46:25'),
('30','M130','0','文章内容','/admin/M130/index','400','1','0','fa-file-text-o','admin','admin','2017-12-26 10:45:36','2018-04-30 14:49:47'),
('31','M131','M130','单页内容','/admin/Single/index','401','0','0','fa-file-o','admin','admin','2017-12-26 10:46:35','2018-04-07 18:46:35'),
('32','M132','M130','列表内容','/admin/Content/index','402','0','0','fa-file-text-o','admin','admin','2017-12-26 10:48:17','2018-04-07 21:52:15'),
('36','M136','M156','定制标签','/admin/Label/index','203','1','1','fa-wrench','admin','admin','2018-01-03 11:52:40','2018-04-07 18:44:31'),
('50','M150','M157','留言信息','/admin/Message/index','501','1','1','fa-question-circle-o','admin','admin','2018-02-01 13:20:17','2018-07-07 23:45:09'),
('51','M151','M157','轮播图片','/admin/Slide/index','502','1','1','fa-picture-o','admin','admin','2018-03-01 14:57:41','2018-04-07 18:47:07'),
('52','M152','M157','友情链接','/admin/Link/index','503','1','1','fa-link','admin','admin','2018-03-01 14:58:45','2018-04-07 18:47:16'),
('53','M153','M156','配置参数','/admin/Config/index','201','1','1','fa-sliders','admin','admin','2018-03-21 14:52:05','2018-04-07 18:44:02'),
('61','M1000','M157','文章内链','/admin/Tags/index','505','1','0','fa-random','admin','admin','2019-07-12 08:25:41','2019-07-12 08:26:23'),
('55','M155','M156','模型管理','/admin/Model/index','204','1','1','fa-codepen','admin','admin','2018-03-25 17:16:06','2018-04-07 18:44:40'),
('56','M156','0','全局配置','/admin/M156/index','200','1','0','fa-globe','admin','admin','2018-03-25 17:20:43','2018-04-30 14:43:56'),
('58','M158','M156','模型字段','/admin/ExtField/index','205','1','1','fa-external-link','admin','admin','2018-03-25 21:24:43','2018-04-07 18:44:49'),
('57','M157','0','扩展内容','/admin/M157/index','500','1','0','fa-arrows-alt','admin','admin','2018-03-25 17:27:57','2018-04-30 14:50:34'),
('60','M160','M157','自定义表单','/admin/Form/index','504','1','1','fa-plus-square-o','admin','admin','2018-05-30 18:25:41','2018-05-31 23:55:10'),
('62','M1001','0','会员中心','/admin/M1001/index','600','1','0','fa-user-o','admin','admin','2019-10-04 08:25:41','2019-10-04 08:26:23'),
('63','M1002','M1001','会员等级','/admin/MemberGroup/index','601','1','0','fa-signal','admin','admin','2019-10-04 08:25:41','2019-10-04 08:26:23'),
('64','M1003','M1001','会员字段','/admin/MemberField/index','602','1','0','fa-wpforms','admin','admin','2019-10-04 08:25:41','2019-10-04 08:26:23'),
('65','M1004','M1001','会员管理','/admin/Member/index','603','1','0','fa-users','admin','admin','2019-10-04 08:25:41','2019-10-04 08:26:23'),
('66','M1005','M1001','文章评论','/admin/MemberComment/index','604','1','0','fa-commenting-o','admin','admin','2019-10-04 08:25:41','2019-10-04 08:26:23');

-- --------------------------------------------------------

--
-- 表的结构 `ay_menu_action`
--

DROP TABLE IF EXISTS `ay_menu_action`;
CREATE TABLE `ay_menu_action` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mcode` varchar(20) NOT NULL COMMENT '菜单编码',
  `action` varchar(20) NOT NULL COMMENT '类型编码',
  PRIMARY KEY (`id`),
  KEY `ay_menu_action_mcode` (`mcode`)
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_menu_action`
--

INSERT INTO `ay_menu_action` (`id`,`mcode`,`action`) VALUES
('1','M102','mod'),
('2','M102','del'),
('3','M102','add'),
('4','M103','mod'),
('5','M103','del'),
('6','M103','add'),
('7','M104','mod'),
('8','M104','del'),
('9','M104','add'),
('10','M105','mod'),
('11','M105','del'),
('12','M105','add'),
('13','M107','mod'),
('14','M107','del'),
('15','M107','add'),
('16','M111','mod'),
('17','M112','mod'),
('18','M114','mod'),
('19','M114','del'),
('20','M114','add'),
('21','M120','mod'),
('22','M120','del'),
('23','M120','add'),
('24','M129','mod'),
('25','M129','del'),
('26','M129','add'),
('27','M131','mod'),
('28','M132','mod'),
('29','M132','del'),
('30','M132','add'),
('31','M136','mod'),
('32','M136','del'),
('33','M136','add'),
('34','M141','mod'),
('35','M141','del'),
('36','M141','add'),
('37','M142','mod'),
('38','M142','del'),
('39','M142','add'),
('40','M143','mod'),
('41','M143','del'),
('42','M143','add'),
('43','M144','mod'),
('44','M144','del'),
('45','M144','add'),
('46','M145','mod'),
('47','M145','del'),
('48','M145','add'),
('49','M150','del'),
('50','M150','mod'),
('51','M151','mod'),
('52','M151','del'),
('53','M151','add'),
('54','M152','mod'),
('55','M152','del'),
('56','M152','add'),
('57','M155','mod'),
('58','M155','del'),
('59','M155','add'),
('60','M158','mod'),
('61','M158','del'),
('62','M158','add'),
('63','M160','add'),
('64','M160','del'),
('65','M160','mod'),
('66','M1000','add'),
('67','M1000','del'),
('68','M1000','mod'),
('69','M1002','add'),
('70','M1002','del'),
('71','M1002','mod'),
('72','M1003','add'),
('73','M1003','del'),
('74','M1003','mod'),
('75','M1004','add'),
('76','M1004','del'),
('77','M1004','mod'),
('78','M1005','del');

-- --------------------------------------------------------

--
-- 表的结构 `ay_message`
--

DROP TABLE IF EXISTS `ay_message`;
CREATE TABLE `ay_message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `acode` varchar(20) NOT NULL COMMENT '区域编码',
  `contacts` varchar(10) DEFAULT NULL COMMENT '联系人',
  `mobile` varchar(12) DEFAULT NULL COMMENT '联系电话',
  `content` varchar(500) DEFAULT NULL COMMENT '留言内容',
  `user_ip` varchar(11) NOT NULL DEFAULT '0' COMMENT 'IP地址',
  `user_os` varchar(30) NOT NULL COMMENT '操作系统',
  `user_bs` varchar(30) NOT NULL COMMENT '浏览器',
  `recontent` varchar(500) NOT NULL COMMENT '回复内容',
  `status` char(1) NOT NULL DEFAULT '1' COMMENT '是否前台显示',
  `create_user` varchar(30) NOT NULL COMMENT '创建人员',
  `update_user` varchar(30) NOT NULL COMMENT '更新人员',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `uid` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ay_message_acode` (`acode`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_message`
--

INSERT INTO `ay_message` (`id`,`acode`,`contacts`,`mobile`,`content`,`user_ip`,`user_os`,`user_bs`,`recontent`,`status`,`create_user`,`update_user`,`create_time`,`update_time`,`uid`) VALUES
('18','cn','123','123','12312','3752288431','Mac','Chrome','','0','guest','guest','2023-05-05 17:13:35','2023-05-05 17:13:35','0'),
('19','cn','123','12312','123123','3752288431','Mac','Chrome','','0','guest','guest','2023-05-05 17:14:44','2023-05-05 17:14:44','0'),
('20','cn','111','15564768866','20230505','466086010','Windows 10','Chrome','','0','guest','guest','2023-05-05 20:53:47','2023-05-05 20:53:47','0'),
('21','cn','测试1','17500084456','ceshi','466086010','Windows 10','Chrome','','0','guest','guest','2023-05-05 20:55:25','2023-05-05 20:55:25','0'),
('22','cn','1','+86 13792366','1','466086010','Windows 10','Chrome','','0','guest','guest','2023-05-05 21:01:25','2023-05-05 21:01:25','0');

-- --------------------------------------------------------

--
-- 表的结构 `ay_model`
--

DROP TABLE IF EXISTS `ay_model`;
CREATE TABLE `ay_model` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '序号',
  `mcode` varchar(20) NOT NULL COMMENT '模型编号',
  `name` varchar(50) NOT NULL COMMENT '模型名称',
  `type` char(1) NOT NULL DEFAULT '2' COMMENT '是否列表类型',
  `urlname` varchar(100) NOT NULL DEFAULT '' COMMENT 'URL名称',
  `listtpl` varchar(50) NOT NULL COMMENT '列表页模板',
  `contenttpl` varchar(50) NOT NULL COMMENT '内容页模板',
  `status` char(1) NOT NULL DEFAULT '1' COMMENT '模型状态',
  `issystem` char(1) NOT NULL DEFAULT '0' COMMENT '系统模型',
  `create_user` varchar(30) NOT NULL COMMENT '创建人员',
  `update_user` varchar(30) NOT NULL COMMENT '更新人员',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ay_model_mcode` (`mcode`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_model`
--

INSERT INTO `ay_model` (`id`,`mcode`,`name`,`type`,`urlname`,`listtpl`,`contenttpl`,`status`,`issystem`,`create_user`,`update_user`,`create_time`,`update_time`) VALUES
('1','1','专题','1','pages','','about.html','1','1','admin','admin','2018-04-11 17:16:01','2019-08-05 11:11:44'),
('2','2','新闻','2','news','newslist.html','news.html','1','1','admin','admin','2018-04-11 17:17:16','2019-08-05 11:12:04'),
('3','3','作品','2','products','productlist.html','product.html','1','0','admin','supersky','2018-04-11 17:17:46','2020-03-15 20:08:08'),
('4','4','案例','2','cases','caselist.html','case.html','1','0','admin','admin','2018-04-11 17:19:53','2019-08-05 11:12:26'),
('5','5','客户','2','cilents','clientlist.html','about.html','1','0','admin','supersky','2018-04-11 17:24:34','2020-03-17 21:15:58');

-- --------------------------------------------------------

--
-- 表的结构 `ay_role`
--

DROP TABLE IF EXISTS `ay_role`;
CREATE TABLE `ay_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色编号',
  `rcode` varchar(20) NOT NULL COMMENT '角色编码',
  `name` varchar(30) NOT NULL COMMENT '角色名称',
  `description` varchar(50) NOT NULL COMMENT '角色描述',
  `create_user` varchar(30) NOT NULL COMMENT '创建人员',
  `update_user` varchar(30) NOT NULL COMMENT '更新人员',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ay_role_rcode` (`rcode`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_role`
--

INSERT INTO `ay_role` (`id`,`rcode`,`name`,`description`,`create_user`,`update_user`,`create_time`,`update_time`) VALUES
('1','R101','系统管理员','系统管理员具有所有权限','admin','admin','2017-03-22 11:33:32','2019-08-05 11:22:02'),
('2','R102','内容管理员','内容管理员具有基本内容管理权限','admin','admin','2017-06-01 00:32:02','2019-08-05 11:22:12');

-- --------------------------------------------------------

--
-- 表的结构 `ay_role_area`
--

DROP TABLE IF EXISTS `ay_role_area`;
CREATE TABLE `ay_role_area` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rcode` varchar(20) NOT NULL,
  `acode` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ay_role_area_rcode` (`rcode`),
  KEY `ay_role_area_acode` (`acode`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_role_area`
--

INSERT INTO `ay_role_area` (`id`,`rcode`,`acode`) VALUES
('3','R101','cn'),
('4','R102','cn');

-- --------------------------------------------------------

--
-- 表的结构 `ay_role_level`
--

DROP TABLE IF EXISTS `ay_role_level`;
CREATE TABLE `ay_role_level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `rcode` varchar(20) NOT NULL COMMENT '角色编码',
  `level` varchar(50) NOT NULL COMMENT '权限地址',
  PRIMARY KEY (`id`),
  KEY `ay_role_level_rcode` (`rcode`)
) ENGINE=MyISAM AUTO_INCREMENT=216 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_role_level`
--

INSERT INTO `ay_role_level` (`id`,`rcode`,`level`) VALUES
('165','R101','/admin/Role/index'),
('164','R101','/admin/Menu/mod'),
('163','R101','/admin/Menu/del'),
('162','R101','/admin/Menu/add'),
('161','R101','/admin/Menu/index'),
('160','R101','/admin/Area/mod'),
('159','R101','/admin/Area/del'),
('158','R101','/admin/Area/add'),
('157','R101','/admin/Area/index'),
('156','R101','/admin/M101/index'),
('155','R101','/admin/Tags/mod'),
('154','R101','/admin/Tags/del'),
('153','R101','/admin/Tags/add'),
('152','R101','/admin/Tags/index'),
('151','R101','/admin/Form/mod'),
('150','R101','/admin/Form/del'),
('149','R101','/admin/Form/add'),
('148','R101','/admin/Form/index'),
('147','R101','/admin/Link/mod'),
('146','R101','/admin/Link/del'),
('145','R101','/admin/Link/add'),
('144','R101','/admin/Link/index'),
('143','R101','/admin/Slide/mod'),
('142','R101','/admin/Slide/del'),
('141','R101','/admin/Slide/add'),
('140','R101','/admin/Slide/index'),
('139','R101','/admin/Message/mod'),
('138','R101','/admin/Message/del'),
('137','R101','/admin/Message/index'),
('136','R101','/admin/M157/index'),
('135','R101','/admin/Content/mod'),
('134','R101','/admin/Content/del'),
('133','R101','/admin/Content/add'),
('132','R101','/admin/Content/index'),
('131','R101','/admin/Single/mod'),
('130','R101','/admin/Single/index'),
('129','R101','/admin/M130/index'),
('128','R101','/admin/ContentSort/mod'),
('127','R101','/admin/ContentSort/del'),
('126','R101','/admin/ContentSort/add'),
('125','R101','/admin/ContentSort/index'),
('124','R101','/admin/Company/mod'),
('123','R101','/admin/Company/index'),
('122','R101','/admin/Site/mod'),
('121','R101','/admin/Site/index'),
('120','R101','/admin/M110/index'),
('119','R101','/admin/ExtField/mod'),
('118','R101','/admin/ExtField/del'),
('117','R101','/admin/ExtField/add'),
('116','R101','/admin/ExtField/index'),
('115','R101','/admin/Model/mod'),
('114','R101','/admin/Model/del'),
('113','R101','/admin/Model/add'),
('112','R101','/admin/Model/index'),
('111','R101','/admin/Label/mod'),
('110','R101','/admin/Label/del'),
('109','R101','/admin/Label/add'),
('108','R101','/admin/Label/index'),
('107','R101','/admin/Config/index'),
('106','R101','/admin/M156/index'),
('205','R102','/admin/Link/add'),
('204','R102','/admin/Link/index'),
('203','R102','/admin/Slide/mod'),
('202','R102','/admin/Slide/del'),
('201','R102','/admin/Slide/add'),
('200','R102','/admin/Slide/index'),
('199','R102','/admin/Message/mod'),
('198','R102','/admin/Message/del'),
('197','R102','/admin/Message/index'),
('196','R102','/admin/M157/index'),
('195','R102','/admin/Content/mod'),
('194','R102','/admin/Content/del'),
('193','R102','/admin/Content/add'),
('192','R102','/admin/Content/index'),
('191','R102','/admin/Single/mod'),
('190','R102','/admin/Single/index'),
('189','R102','/admin/M130/index'),
('188','R102','/admin/ContentSort/mod'),
('187','R102','/admin/ContentSort/del'),
('186','R102','/admin/ContentSort/add'),
('185','R102','/admin/ContentSort/index'),
('184','R102','/admin/Company/mod'),
('183','R102','/admin/Company/index'),
('182','R102','/admin/Site/mod'),
('181','R102','/admin/Site/index'),
('180','R102','/admin/M110/index'),
('166','R101','/admin/Role/add'),
('167','R101','/admin/Role/del'),
('168','R101','/admin/Role/mod'),
('169','R101','/admin/User/index'),
('170','R101','/admin/User/add'),
('171','R101','/admin/User/del'),
('172','R101','/admin/User/mod'),
('173','R101','/admin/Syslog/index'),
('174','R101','/admin/Type/index'),
('175','R101','/admin/Type/add'),
('176','R101','/admin/Type/del'),
('177','R101','/admin/Type/mod'),
('178','R101','/admin/Database/index'),
('179','R101','/admin/Site/server'),
('206','R102','/admin/Link/del'),
('207','R102','/admin/Link/mod'),
('208','R102','/admin/Form/index'),
('209','R102','/admin/Form/add'),
('210','R102','/admin/Form/del'),
('211','R102','/admin/Form/mod'),
('212','R102','/admin/Tags/index'),
('213','R102','/admin/Tags/add'),
('214','R102','/admin/Tags/del'),
('215','R102','/admin/Tags/mod');

-- --------------------------------------------------------

--
-- 表的结构 `ay_site`
--

DROP TABLE IF EXISTS `ay_site`;
CREATE TABLE `ay_site` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '站点编号',
  `acode` varchar(20) NOT NULL COMMENT '区域代码',
  `title` varchar(100) NOT NULL COMMENT '站点标题',
  `subtitle` varchar(200) NOT NULL COMMENT '站点副标题',
  `domain` varchar(50) NOT NULL COMMENT '站点地址',
  `logo` varchar(100) NOT NULL COMMENT '站点LOGO地址',
  `keywords` varchar(200) NOT NULL COMMENT '站点关键字',
  `description` varchar(500) NOT NULL COMMENT '站点描述',
  `icp` varchar(30) NOT NULL COMMENT '站点备案',
  `theme` varchar(30) NOT NULL COMMENT '站点主题',
  `statistical` varchar(500) NOT NULL COMMENT '站点统计码',
  `copyright` varchar(200) NOT NULL COMMENT '版权信息',
  PRIMARY KEY (`id`),
  KEY `ay_site_acode` (`acode`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_site`
--

INSERT INTO `ay_site` (`id`,`acode`,`title`,`subtitle`,`domain`,`logo`,`keywords`,`description`,`icp`,`theme`,`statistical`,`copyright`) VALUES
('1','cn','济宁网站制作-济宁网络公司-济宁做网站-济宁建网站-济宁网站建设-济宁网络推广公司-网站建设-微信平台开发-APP开发-小程序开发-软件开发','济宁主舵者网络科技有限公司','www.eskystudio.com','/static/upload/image/20230130/1675046557159856.png','济宁网络推广,济宁网站建设,济宁seo优化,济宁网站优化,济宁网络推广公司,微信平台开发,APP开发,小程序开发,软件开发,济宁做网站,济宁建网站,济宁网站制作,济宁网络公司','济宁主舵者网络科技有限公司专注于品牌网站建设，原创设计与网络营销相结合，一切工作为创造企业价值而努力，用十年的专业经验，专心为您服务！24小时热线：15564768866电话：0537-2254866　地址：济宁市高新区金宇路红星美凯龙商业中心518室','鲁ICP备19021613号-3','eskystudio','','Copyright © 2018-2020 Eskystudio.com All Rights Reserved.');

-- --------------------------------------------------------

--
-- 表的结构 `ay_slide`
--

DROP TABLE IF EXISTS `ay_slide`;
CREATE TABLE `ay_slide` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '序号',
  `acode` varchar(20) NOT NULL COMMENT '区域编码',
  `gid` int(10) unsigned NOT NULL COMMENT '分组序号',
  `pic` varchar(100) NOT NULL COMMENT '图片地址',
  `link` varchar(100) NOT NULL COMMENT '跳转链接',
  `title` varchar(50) NOT NULL COMMENT '说明文字',
  `subtitle` varchar(100) NOT NULL COMMENT '副标题/描述',
  `sorting` int(11) NOT NULL COMMENT '排序',
  `create_user` varchar(30) NOT NULL COMMENT '创建人员',
  `update_user` varchar(30) NOT NULL COMMENT '更新人员',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `ay_slide_acode` (`acode`),
  KEY `ay_slide_gid` (`gid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_slide`
--

INSERT INTO `ay_slide` (`id`,`acode`,`gid`,`pic`,`link`,`title`,`subtitle`,`sorting`,`create_user`,`update_user`,`create_time`,`update_time`) VALUES
('1','cn','1','/images/ban4.jpg','/static/upload/image/20211012/1634026273661796.jpg','用营销的思维做设计 让艺术为商业服务','我们注重品牌网站的高端视觉效果 也一样重视营销，因为设计不仅仅是表面上的视觉呈现，更是深层次的品牌与消费者之间的沟通，用心设计，用图说话；我们基于原则去实践，用责任心和远见塑造未来','1','admin','supersky','2018-03-01 16:19:03','2021-10-12 16:11:30'),
('2','cn','1','/images/ban3.jpg','/images/banner3.jpg','将技术转变为生产力 而不是单纯的炫酷','技术作为网站设计的最基础环节，我们需要时刻保持内容的革新，无论何时，都必须让我们的客户获得最前沿的技术支持，辅助客户品牌以呈现给消费者完美的用户体验','2','admin','supersky','2018-04-12 10:46:07','2021-10-12 16:09:57'),
('3','cn','1','/images/ban2.jpg','/images/banner2.jpg','响你所想的极致体验 高端就是响应式','专业深耕响应式高端网站建设技术，质量保证、维护简单，利用真正的响应式引擎技术实现一个网站同时支持PC、平板电脑、手机全终端浏览，做真正有用的高端网站','3','supersky','supersky','2020-03-13 17:00:53','2021-10-12 16:09:57'),
('4','cn','1','/static/upload/image/20211012/1634026181826141.jpg','/images/banner.jpg','不断学习与突破 总是站在行业的前沿','我们对每一位团队成员精挑细选，要求每一位成员都做自己思想的主人，努力打造一支最前沿的响应式网站建设的高端技术团队，因为我们深知：视觉更广 眼界更宽 服务才更出色!','4','supersky','supersky','2020-03-13 17:01:19','2021-10-12 16:09:57');

-- --------------------------------------------------------

--
-- 表的结构 `ay_syslog`
--

DROP TABLE IF EXISTS `ay_syslog`;
CREATE TABLE `ay_syslog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '日志编号',
  `level` varchar(20) NOT NULL COMMENT '信息等级',
  `event` varchar(200) NOT NULL COMMENT '事件',
  `user_ip` varchar(11) NOT NULL DEFAULT '0' COMMENT '客户端IP',
  `user_os` varchar(30) NOT NULL COMMENT '客户端系统',
  `user_bs` varchar(30) NOT NULL COMMENT '客户端浏览器',
  `create_user` varchar(30) NOT NULL COMMENT '创建人员',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_syslog`
--

INSERT INTO `ay_syslog` (`id`,`level`,`event`,`user_ip`,`user_os`,`user_bs`,`create_user`,`create_time`) VALUES
('1','info','修改文章295成功！','3752288431','Mac','Chrome','test','2023-05-05 20:38:56'),
('2','info','登录成功!','1739201058','Windows 10','Chrome','test','2023-05-05 20:43:39'),
('3','info','修改扩展字段4成功！','3752288431','Mac','Chrome','test','2023-05-05 20:45:42'),
('4','info','修改扩展字段5成功！','3752288431','Mac','Chrome','test','2023-05-05 20:45:52'),
('5','info','修改单页内容46成功！','3752288431','Mac','Chrome','test','2023-05-05 20:48:36'),
('6','info','备份数据库成功！','466086010','Windows 10','Chrome','supersky','2023-05-05 20:49:26'),
('7','info','other-bot爬行/?site-case/177.html','908367050','Other','Other','','2023-05-05 20:49:42'),
('8','info','other-bot爬行/?site-case/177.html','908367155','Other','Other','','2023-05-05 20:49:53'),
('9','info','修改文章58成功！','3752288431','Mac','Chrome','test','2023-05-05 20:50:01'),
('10','info','修改文章58成功！','3752288431','Mac','Chrome','test','2023-05-05 20:50:28'),
('11','info','修改文章178成功！','3752288431','Mac','Chrome','test','2023-05-05 20:52:03'),
('12','info','留言提交成功！','466086010','Windows 10','Chrome','supersky','2023-05-05 20:53:47'),
('13','info','留言提交成功！','466086010','Windows 10','Chrome','supersky','2023-05-05 20:55:25'),
('14','info','登录成功!','3752288431','Mac','Firefox','test','2023-05-05 20:57:55'),
('15','info','登录成功!','3729174777','Mac','Chrome','test','2023-05-05 20:58:40'),
('16','info','留言提交成功！','466086010','Windows 10','Chrome','supersky','2023-05-05 21:01:25'),
('17','info','other-bot爬行/?site-case/65.html','908367082','Other','Other','','2023-05-05 21:06:18'),
('18','info','other-bot爬行/?site-case/65.html','908367077','Other','Other','','2023-05-05 21:06:39'),
('19','info','新增文章成功！','3752288431','Mac','Firefox','test','2023-05-05 21:07:07'),
('20','info','other-bot爬行/?news_21_5/','908366987','Other','Other','','2023-05-05 21:22:31'),
('21','info','other-bot爬行/?news_21_5/','908367140','Other','Other','','2023-05-05 21:22:35'),
('22','info','批量删除文章成功！','466086010','Windows 10','Chrome','supersky','2023-05-05 21:22:43'),
('23','info','批量删除文章成功！','466086010','Windows 10','Chrome','supersky','2023-05-05 21:23:24');

-- --------------------------------------------------------

--
-- 表的结构 `ay_tags`
--

DROP TABLE IF EXISTS `ay_tags`;
CREATE TABLE `ay_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `acode` varchar(20) NOT NULL COMMENT '区域',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `link` varchar(200) NOT NULL COMMENT '链接',
  `create_user` varchar(30) NOT NULL COMMENT '添加人员',
  `update_user` varchar(30) NOT NULL COMMENT '更新人员',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `ay_tags_acode` (`acode`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_tags`
--

INSERT INTO `ay_tags` (`id`,`acode`,`name`,`link`,`create_user`,`update_user`,`create_time`,`update_time`) VALUES
('1','cn','济宁网站建设','https://www.eskystudio.com','admin','supersky','2019-07-12 14:33:13','2020-04-08 09:22:21'),
('2','cn','网站','http://www.eskystudio.com','supersky','supersky','2020-04-09 14:33:29','2020-04-09 14:33:29'),
('3','cn','网络推广','http://www.eskystudio.com/','supersky','supersky','2020-04-09 14:33:43','2020-04-09 14:33:43'),
('4','cn','微信平台','http://www.eskystudio.com/','supersky','supersky','2020-04-09 14:33:49','2020-04-09 14:33:49'),
('5','cn','济宁做网站','https://www.eskystudio.com/','supersky','supersky','2020-04-11 11:04:40','2020-04-11 11:04:40'),
('6','cn','小程序','http://www.eskystudio.com/','supersky','supersky','2020-04-18 16:35:07','2020-04-18 16:35:07'),
('7','cn','商城','http://www.eskystudio.com','supersky','supersky','2020-04-18 16:44:31','2020-04-18 16:44:31'),
('8','cn','小程序建设','http://www.eskysoft.cn','supersky','supersky','2020-07-22 19:42:39','2020-07-22 19:42:39'),
('9','cn','主舵者','http://www.jnzdz.cn','supersky','supersky','2020-07-22 19:42:56','2020-07-22 19:42:56'),
('10','cn','济宁网络公司','http://www.eskystudio.com','supersky','supersky','2020-08-11 16:33:07','2020-08-11 16:33:07'),
('11','cn','小程序定制开发','http://www.eskystudio.com','supersky','supersky','2023-02-20 10:48:05','2023-02-20 10:48:55'),
('12','cn','微信小程序开发','https://www.eskystudio.com/','supersky','supersky','2023-02-20 10:55:44','2023-02-20 10:55:44'),
('13','cn','商城小程序定制开发','https://www.eskystudio.com','supersky','supersky','2023-02-20 10:59:24','2023-02-20 10:59:24');

-- --------------------------------------------------------

--
-- 表的结构 `ay_type`
--

DROP TABLE IF EXISTS `ay_type`;
CREATE TABLE `ay_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '类型编号',
  `tcode` varchar(20) NOT NULL COMMENT '类型编码',
  `name` varchar(30) NOT NULL COMMENT '类型名称',
  `item` varchar(30) NOT NULL COMMENT '类型项',
  `value` varchar(20) NOT NULL DEFAULT '0' COMMENT '类型值',
  `sorting` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `create_user` varchar(30) NOT NULL COMMENT '添加人员',
  `update_user` varchar(30) NOT NULL COMMENT '更新时间',
  `create_time` datetime NOT NULL COMMENT '添加时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `ay_type_tcode` (`tcode`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_type`
--

INSERT INTO `ay_type` (`id`,`tcode`,`name`,`item`,`value`,`sorting`,`create_user`,`update_user`,`create_time`,`update_time`) VALUES
('1','T101','菜单功能','新增','add','1','admin','admin','2017-04-27 07:28:34','2017-08-09 15:25:56'),
('2','T101','菜单功能','删除','del','2','admin','admin','2017-04-27 07:29:08','2017-08-09 15:23:34'),
('3','T101','菜单功能','修改','mod','3','admin','admin','2017-04-27 07:29:34','2017-08-09 15:23:32'),
('4','T101','菜单功能','导出','export','4','admin','admin','2017-04-27 07:30:42','2017-08-09 15:23:29'),
('5','T101','菜单功能','导入','import','5','admin','admin','2017-04-27 07:31:38','2017-08-09 15:23:27');

-- --------------------------------------------------------

--
-- 表的结构 `ay_user`
--

DROP TABLE IF EXISTS `ay_user`;
CREATE TABLE `ay_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户编号',
  `ucode` varchar(20) NOT NULL COMMENT '用户编码',
  `username` varchar(30) NOT NULL COMMENT '用户账号',
  `realname` varchar(30) NOT NULL COMMENT '真实名字',
  `password` char(32) NOT NULL COMMENT '用户密码',
  `status` char(1) NOT NULL DEFAULT '1' COMMENT '是否启用',
  `login_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `last_login_ip` varchar(11) NOT NULL DEFAULT '0' COMMENT '最后登录IP',
  `create_user` varchar(30) NOT NULL COMMENT '添加人员',
  `update_user` varchar(30) NOT NULL COMMENT '更新用户',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ay_user_ucode` (`ucode`),
  KEY `ay_user_username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_user`
--

INSERT INTO `ay_user` (`id`,`ucode`,`username`,`realname`,`password`,`status`,`login_count`,`last_login_ip`,`create_user`,`update_user`,`create_time`,`update_time`) VALUES
('1','10001','supersky','Eskystudio','ae8af5aaeed147e2908443a3f5fcc741','1','156','466086010','admin','supersky','2017-05-08 18:50:30','2023-05-05 20:24:08'),
('2','10002','test','test','14e1b600b1fd579f47433b88e8d85291','1','9','3729174777','supersky','supersky','2021-12-22 16:12:52','2023-05-05 20:58:40');

-- --------------------------------------------------------

--
-- 表的结构 `ay_user_role`
--

DROP TABLE IF EXISTS `ay_user_role`;
CREATE TABLE `ay_user_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `ucode` varchar(20) NOT NULL COMMENT '用户编码',
  `rcode` varchar(20) NOT NULL COMMENT '角色编码',
  PRIMARY KEY (`id`),
  KEY `ay_user_role_ucode` (`ucode`),
  KEY `ay_user_role_rcode` (`rcode`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ay_user_role`
--

INSERT INTO `ay_user_role` (`id`,`ucode`,`rcode`) VALUES
('1','10001','R101'),
('2','10002','R101');

-- --------------------------------------------------------

