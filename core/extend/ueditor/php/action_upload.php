<?php
/**
 * 上传附件和上传视频
 * User: Jinqn
 * Date: 14-04-09
 * Time: 上午10:17
 */

include "Uploader.class.php";
include('../../qcloud/autoload.php');
/* 上传配置 */
$base64 = "upload";
switch (htmlspecialchars($_GET['action'])) {
    case 'uploadimage':
        $config = array(
            "pathFormat" => $CONFIG['imagePathFormat'],
            "maxSize" => $CONFIG['imageMaxSize'],
            "allowFiles" => $CONFIG['imageAllowFiles']
        );
        $fieldName = $CONFIG['imageFieldName'];
        break;
    case 'uploadscrawl':
        $config = array(
            "pathFormat" => $CONFIG['scrawlPathFormat'],
            "maxSize" => $CONFIG['scrawlMaxSize'],
            "allowFiles" => $CONFIG['scrawlAllowFiles'],
            "oriName" => "scrawl.png"
        );
        $fieldName = $CONFIG['scrawlFieldName'];
        $base64 = "base64";
        break;
    case 'uploadvideo':
        $config = array(
            "pathFormat" => $CONFIG['videoPathFormat'],
            "maxSize" => $CONFIG['videoMaxSize'],
            "allowFiles" => $CONFIG['videoAllowFiles']
        );
        $fieldName = $CONFIG['videoFieldName'];
        break;
    case 'uploadfile':
    default:
        $config = array(
            "pathFormat" => $CONFIG['filePathFormat'],
            "maxSize" => $CONFIG['fileMaxSize'],
            "allowFiles" => $CONFIG['fileAllowFiles']
        );
        $fieldName = $CONFIG['fileFieldName'];
        break;
}

if (defined('STATIC_DIR')) {
    $config['pathFormat'] = STATIC_DIR . $config['pathFormat'];
}


if (true) {
    $file_name = $_FILES["upfile"]["name"];
    $src = $_FILES["upfile"]["tmp_name"];

    $fnarray=explode('.', $file_name);

    $file_suffix = strtolower(end($fnarray)); //后缀名

    $dst = date('YmdHis').rand(1,999).'.'.$file_suffix;

    $cosClient = new \Qcloud\Cos\Client(array(
            'region' => ENDPIINT, #地域，如ap-guangzhou,ap-beijing-1
            'credentials' => array(
                'secretId' => SECRETID,
                'secretKey' => SECRETKEY,
            ),
        )
    );

    // 若初始化 Client 时未填写 appId，则 bucket 的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
    //$bucket = 'file-1317956618';
    try {
        $result = $cosClient->upload(
            $bucket = BUCKET,
            $key = $dst,
            $body = fopen($src, 'rb')
        );

        $url = $result['Location'];

        $url_p=str_replace(COS_URL, '', $url);

        $arr = [
            'state' => 'SUCCESS',
            'url' => $url_p,
            'title' => $dst,
            'original' => $file_name,
            'type' => '.'.$file_suffix,
            'size' => $_FILES["upfile"]['size']
        ];
        return json_encode($arr);

    } catch (\Exception $e) {

        $arr = [
            'state' => 'error',
            'msg' => $e->getMessage()
        ];
        return json_encode($arr);

        $rs['msg'] = $e->getMessage();

        return $rs;
    }
}

/* 生成上传实例对象并完成上传 */
$up = new Uploader($fieldName, $config, $base64);

// 图片打水印
$rs = $up->getFileInfo();
$ext = array(
    '.jpg',
    '.png',
    '.gif'
);
if (in_array($rs['type'], $ext)) {
    resize_img(ROOT_PATH . $rs['url']); // 缩放大小
    watermark_img(ROOT_PATH . $rs['url']); // 水印
}

/**
 * 得到上传文件所对应的各个参数,数组结构
 * array(
 * "state" => "", //上传状态，上传成功时必须返回"SUCCESS"
 * "url" => "", //返回的地址
 * "title" => "", //新文件名
 * "original" => "", //原始文件名
 * "type" => "" //文件类型
 * "size" => "", //文件大小
 * )
 */

/* 返回数据 */
return json_encode($up->getFileInfo());
