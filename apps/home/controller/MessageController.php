<?php
/**
 * @copyright (C)2020-2099 Hnaoyun Inc.
 * @author XingMeng
 * @email hnxsh@foxmail.com
 * @date 2020年3月8日
 *  留言控制器
 */
namespace app\home\controller;

use app\home\model\DoModel;
use core\basic\Controller;
use app\home\model\ParserModel;
use core\basic\Url;
use mysql_xdevapi\Table;

class MessageController extends Controller
{

    protected $model;

    public function __construct()
    {
        $this->model = new ParserModel();
    }

    // 留言新增
    public function index134()
    {
        $type = get('type') ?? '';
        if (isset($type) && $type == 'getMsg') {
            $model = new DoModel();

            $message = $model->getMsg();

            if (!empty($message)) {
                foreach ($message as $k => $v) {
                    $message[$k]->image = $v->image != '' ? IMAGES_PATH. $v->image : '';
                    $message[$k]->video = $v->video != '' ? VIDEO_PATH. $v->video : '';
                }
            }

            json(1, $message);exit();
        }
        $file = $_FILES['image'];
        $video = $_FILES['video'];

        if (!empty($file['name'])) {
            $this->uploadFiles($file, 1);
        }

        if (!empty($video['name'])) {
            $this->uploadFiles($video, 2);
        }

        $contacts = post('contacts');
        $mobile = post('mobile');
        $email = post('email');
//        $image = file('image');


        $preg_phone='^1(?:3[0-9]|5[012356789]|8[0256789]|7[0678])(?P&lt;separato&gt;-?)\d{4}(?P=separato)\d{4}$';

//        $preg_phone='/^1[34578]\d{9}$/';
        if(!preg_match($preg_phone,$mobile)){
            alert_back('手机号必须为11位数字');
        }

        if (mb_strlen($contacts) > 50) {
            alert_back('姓名不能超过50个字节！');
        }
        if (!checkEmail($email)) {
            alert_back('邮箱格式不正确！');
        }

        if ($this->config('message_status') === '0') {
            error('系统已经关闭留言功能，请到后台开启再试！');
        }

        if (time() - session('lastsub') < 10) {
            alert_back('您提交太频繁了，请稍后再试！');
        }

        // 需登录
        if ($this->config('message_rqlogin') && ! session('pboot_uid')) {
            if (! ! $backurl = $_SERVER['HTTP_REFERER']) {
                alert_location("请先注册登录后再留言！", Url::home('member/login', null, "backurl=" . urlencode($backurl)));
            } else {
                alert_location("请先注册登录后再留言！", Url::home('member/login'));
            }
        }

        // 验证码验证
        $checkcode = strtolower(post('checkcode', 'var'));
        if ($this->config('message_check_code') !== '0') {
            if (! $checkcode) {
                alert_back('验证码不能为空！');
            }

            if ($checkcode != session('checkcode')) {
                alert_back('验证码错误！');
            }
        }

        // 读取字段
        if (! $form = $this->model->getFormField(1)) {
            alert_back('留言表单不存在任何字段，请核对后重试！');
        }

        // 接收数据
        $mail_body = '';
        foreach ($form as $value) {
            $field_data = post($value->name);
            if (is_array($field_data)) { // 如果是多选等情况时转换
                $field_data = implode(',', $field_data);
            }
            $field_data = preg_replace_r('/pboot:if/i', '', $field_data);
            if ($value->required && ! $field_data) {
                alert_back($value->description . '不能为空！');
            } else {
                $data[$value->name] = $field_data;
                $mail_body .= $value->description . '：' . $field_data . '<br>';
            }
        }

        $status = $this->config('message_verify') === '0' ? 1 : 0;

        $data['image'] = $file['name'];
        $data['video'] = $video['name'];
        $data['email'] = $email;
        // 设置额外数据
        if ($data) {
            $data['acode'] = get_lg();
            $data['user_ip'] = ip2long(get_user_ip());
            $data['user_os'] = get_user_os();
            $data['user_bs'] = get_user_bs();
            $data['recontent'] = '';
            $data['status'] = $status;
            $data['create_user'] = 'guest';
            $data['update_user'] = 'guest';
            $data['uid'] = session('pboot_uid');
        }

        if ($this->model->addMessage($data)) {
            session('lastsub', time()); // 记录最后提交时间
            $this->log('留言提交成功！');
            if ($this->config('message_send_mail') && $this->config('message_send_to')) {
                $mail_subject = "【" . CMSNAME . "】您有新的" . $value->form_name . "信息，请注意查收！";
                $mail_body .= '<br>来自网站 ' . get_http_url() . ' （' . date('Y-m-d H:i:s') . '）';
                sendmail($this->config(), $this->config('message_send_to'), $mail_subject, $mail_body);
            }
            alert_location('提交成功！', '-1', 1);
        } else {
            $this->log('留言提交失败！');
            alert_back('提交失败！');
        }

    }
    public function index()
    {
        if ($_POST) {

             print_r($_POST);die;
            $file = $_FILES['image'];
            $video = $_FILES['video'];

            if (!empty($file['name'])) {
                $this->uploadFiles($file, 1);
            }

            if (!empty($video['name'])) {
                $this->uploadFiles($video, 2);
            }

            $contacts = post('contacts');
            $mobile = post('mobile');
            $email = post('email');
            //        $image = file('image');


            $preg_phone='^1(?:3[0-9]|5[012356789]|8[0256789]|7[0678])(?P&lt;separato&gt;-?)\d{4}(?P=separato)\d{4}$';

            //        $preg_phone='/^1[34578]\d{9}$/';
            if(!preg_match($preg_phone,$mobile)){
                alert_back('手机号必须为11位数字');
            }

            if (mb_strlen($contacts) > 50) {
                alert_back('姓名不能超过50个字节！');
            }
            if (!checkEmail($email)) {
                alert_back('邮箱格式不正确！');
            }


            if ($this->config('message_status') === '0') {
                error('系统已经关闭留言功能，请到后台开启再试！');
            }

            if (time() - session('lastsub') < 10) {
                alert_back('您提交太频繁了，请稍后再试！');
            }

            // 需登录
            if ($this->config('message_rqlogin') && ! session('pboot_uid')) {
                if (! ! $backurl = $_SERVER['HTTP_REFERER']) {
                    alert_location("请先注册登录后再留言！", Url::home('member/login', null, "backurl=" . urlencode($backurl)));
                } else {
                    alert_location("请先注册登录后再留言！", Url::home('member/login'));
                }
            }

            // 验证码验证
            $checkcode = strtolower(post('checkcode', 'var'));
            if ($this->config('message_check_code') !== '0') {
                if (! $checkcode) {
                    alert_back('验证码不能为空！');
                }

                if ($checkcode != session('checkcode')) {
                    alert_back('验证码错误！');
                }
            }

            // 读取字段
            if (! $form = $this->model->getFormField(1)) {
                alert_back('留言表单不存在任何字段，请核对后重试！');
            }

            echo 123;die;
            // 接收数据
            $mail_body = '';
            foreach ($form as $value) {
                $field_data = post($value->name);
                if (is_array($field_data)) { // 如果是多选等情况时转换
                    $field_data = implode(',', $field_data);
                }
                $field_data = preg_replace_r('/pboot:if/i', '', $field_data);
                if ($value->required && ! $field_data) {
                    alert_back($value->description . '不能为空！');
                } else {
                    $data[$value->name] = $field_data;
                    $mail_body .= $value->description . '：' . $field_data . '<br>';
                }
            }

            $status = $this->config('message_verify') === '0' ? 1 : 0;

            $data['image'] = $file['name'];
            $data['video'] = $video['name'];
            $data['email'] = $email;
            // 设置额外数据
            if ($data) {
                $data['acode'] = get_lg();
                $data['user_ip'] = ip2long(get_user_ip());
                $data['user_os'] = get_user_os();
                $data['user_bs'] = get_user_bs();
                $data['recontent'] = '';
                $data['status'] = $status;
                $data['create_user'] = 'guest';
                $data['update_user'] = 'guest';
                $data['uid'] = session('pboot_uid');
            }

            echo 123;die;
            if ($this->model->addMessage($data)) {
                session('lastsub', time()); // 记录最后提交时间
                $this->log('留言提交成功！');
                if ($this->config('message_send_mail') && $this->config('message_send_to')) {
                    $mail_subject = "【" . CMSNAME . "】您有新的" . $value->form_name . "信息，请注意查收！";
                    $mail_body .= '<br>来自网站 ' . get_http_url() . ' （' . date('Y-m-d H:i:s') . '）';
                    sendmail($this->config(), $this->config('message_send_to'), $mail_subject, $mail_body);
                }
                alert_location('提交成功！', '-1', 1);
            } else {
                $this->log('留言提交失败！');
                alert_back('提交失败！');
            }
        }

//        if ($_GET) {
//            $model = new DoModel();
//
//            $message = $model->getMsg();
//
//            if (!empty($message)) {
//                foreach ($message as $k => $v) {
//                    $message[$k]->image = $v->image != '' ? IMAGES_PATH. $v->image : '';
//                    $message[$k]->video = $v->video != '' ? VIDEO_PATH. $v->video : '';
//                }
//            }
//
//            json(1, $message);exit();
//        }
    }


    public function uploadFiles($file, $fileType = 1)
    {
        //得到传输的数据
        //得到文件名称
        $name = $file['name'];
        $type = strtolower(substr($name,strrpos($name,'.')+1)); //得到文件类型，并且都转化成小写
        $allow_type = array('jpg','jpeg','gif','png', 'mp4'); //定义允许上传的类型


        if ($fileType == 1 && !in_array($type, $allow_type)) {
            alert_back( '上传图片必须为 jpg、jpeg、gif或png格式');
        }

        if ($fileType == 2 && $type != 'mp4') {
            alert_back( '上传视频必须为 mp4 格式');
        }

        //判断文件类型是否被允许上传
        if(!in_array($type, $allow_type)){
            //如果不被允许，则直接停止程序运行
//            alert_back('上传类型有误！');
        }
        //判断是否是通过HTTP POST上传的
        if(!is_uploaded_file($file['tmp_name'])){
            //如果不是通过HTTP POST上传的
//            alert_back( '上传方式错误');
        }
        $upload_path = $fileType == 1 ? UPLOADS_IMAGES_PATH : UPLOADS_VIDEO_PATH; //上传文件的存放路径
        //开始移动文件到相应的文件夹
        if(move_uploaded_file($file['tmp_name'],$upload_path.$file['name'])){
            return 1;
        }else{
            alert_back('图片或视频上传失败，请重新填写留言');
        }
    }


    public function msgPost()
    {
        $file = $_FILES['image'];
        $video = $_FILES['video'];

        $param = $_POST;
//        print_r($_POST);die;
        if (!empty($file['name'])) {
            $this->uploadFiles($file, 1);
        }

        if (!empty($video['name'])) {
            $this->uploadFiles($video, 2);
        }

        $contacts = post('contacts');
        $mobile = post('mobile');
        $email = post('email');
        //        $image = file('image');

//print_r($_POST);die;
        $preg_phone='^1(?:3[0-9]|5[012356789]|8[0256789]|7[0678])(?P&lt;separato&gt;-?)\d{4}(?P=separato)\d{4}$';

        //        $preg_phone='/^1[34578]\d{9}$/';
//        if(!preg_match($preg_phone,$param['mobile'])){
//            alert_back('手机号必须为11位数字');
//        }

        if (mb_strlen($contacts) > 50) {
            alert_back('姓名不能超过50个字节！');
        }
//        if (!checkEmail($param['email'])) {
//            alert_back('邮箱格式不正确！');
//        }


        if ($this->config('message_status') === '0') {
            error('系统已经关闭留言功能，请到后台开启再试！');
        }

        if (time() - session('lastsub') < 10) {
            alert_back('您提交太频繁了，请稍后再试！');
        }

        $data['image'] = $file['name'];
        $data['video'] = $video['name'];
        $data['contacts'] = filterateVariable($param['contacts']);
        $data['mobile'] = filterateVariable($param['mobile']);
        $data['email'] = filterateVariable($param['email']);
        $data['title'] = filterateVariable($param['title']);
        $data['content'] = ($param['content']);
        // 设置额外数据
        if ($data) {
            $data['acode'] = get_lg();
            $data['user_ip'] = ip2long(get_user_ip());
            $data['user_os'] = get_user_os();
            $data['user_bs'] = get_user_bs();
            $data['recontent'] = '';
            $data['status'] = 0;
            $data['create_user'] = 'guest';
            $data['update_user'] = 'guest';
            $data['uid'] = session('pboot_uid');
        }

//        print_r($data);die;

        if ($this->model->addMessage($data)) {
            alert_location('提交成功！', '-1', 1);
        } else {
            $this->log('留言提交失败！');
            alert_back('提交失败！');
        }
    }

    public function getMsg()
    {
        $model = new DoModel();

        $message = $model->getMsg();

        if (!empty($message)) {
            foreach ($message as $k => $v) {
                $message[$k]->image = $v->image != '' ? IMAGES_PATH. $v->image : '';
                $message[$k]->video = $v->video != '' ? VIDEO_PATH. $v->video : '';
            }
        }

        json(1, $message);exit();
    }

    public function getMsgById()
    {
        $id = get('id');

        $mode = new DoModel();
        $data = $mode->getMsgById($id);

//        print_r($data->title);die;
        $this->assign('title',  $data->title);
        $this->assign('create_time',  $data->create_time);
        $this->assign('contacts',  $data->contacts);
        $this->assign('content',  $data->content);
        $this->assign('image',  $data->image);
        $this->assign('video',  $data->video);


        $this->display('msgdetail.html');
    }
}
